/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faqi.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.faqi.entity.Faqi;
import com.jeesite.modules.faqi.dao.FaqiDao;

/**
 * 发起Service
 * @author 曾
 * @version 2019-02-18
 */
@Service
@Transactional(readOnly=true)
public class FaqiService extends CrudService<FaqiDao, Faqi> {
	
	/**
	 * 获取单条数据
	 * @param faqi
	 * @return
	 */
	@Override
	public Faqi get(Faqi faqi) {
		return super.get(faqi);
	}
	
	/**
	 * 查询分页数据
	 * @param faqi 查询条件
	 * @param faqi.page 分页对象
	 * @return
	 */
	@Override
	public Page<Faqi> findPage(Faqi faqi) {
		return super.findPage(faqi);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param faqi
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Faqi faqi) {
		super.save(faqi);
	}
	
	/**
	 * 更新状态
	 * @param faqi
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Faqi faqi) {
		super.updateStatus(faqi);
	}
	
	/**
	 * 删除数据
	 * @param faqi
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Faqi faqi) {
		super.delete(faqi);
	}
	
}