/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.arrangeclass.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.arrangeclass.entity.ArrangeClass;
import com.jeesite.modules.arrangeclass.service.ArrangeClassService;

/**
 * ArrangeclassController
 * @author liuchang
 * @version 2019-02-19
 */
@Controller
@RequestMapping(value = "${adminPath}/arrangeclass/arrangeClass")
public class ArrangeClassController extends BaseController {

	@Autowired
	private ArrangeClassService arrangeClassService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public ArrangeClass get(String id, boolean isNewRecord) {
		return arrangeClassService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("arrangeclass:arrangeClass:view")
	@RequestMapping(value = {"list", ""})
	public String list(ArrangeClass arrangeClass, Model model) {
		model.addAttribute("arrangeClass", arrangeClass);
		return "modules/arrangeclass/arrangeClassList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("arrangeclass:arrangeClass:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<ArrangeClass> listData(ArrangeClass arrangeClass, HttpServletRequest request, HttpServletResponse response) {
		arrangeClass.setPage(new Page<>(request, response));
		Page<ArrangeClass> page = arrangeClassService.findPage(arrangeClass);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("arrangeclass:arrangeClass:view")
	@RequestMapping(value = "form")
	public String form(ArrangeClass arrangeClass, Model model) {
		model.addAttribute("arrangeClass", arrangeClass);
		return "modules/arrangeclass/arrangeClassForm";
	}

	/**
	 * 保存Arrangeclass
	 */
	@RequiresPermissions("arrangeclass:arrangeClass:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated ArrangeClass arrangeClass) {
		arrangeClassService.save(arrangeClass);
		return renderResult(Global.TRUE, text("保存Arrangeclass成功！"));
	}
	
	/**
	 * 删除Arrangeclass
	 */
	@RequiresPermissions("arrangeclass:arrangeClass:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(ArrangeClass arrangeClass) {
		arrangeClassService.delete(arrangeClass);
		return renderResult(Global.TRUE, text("删除Arrangeclass成功！"));
	}
	
}