/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.web;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.collect.MapUtils;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.idgen.IdGen;
import com.jeesite.modules.sys.utils.UserUtils;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.sys.entity.Caidan;
import com.jeesite.modules.sys.service.CaidanService;

/**
 * 菜单表Controller
 * @author 曾
 * @version 2019-02-13
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/caidan")
public class CaidanController extends BaseController {

	@Autowired
	private CaidanService caidanService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Caidan get(String menuCode, boolean isNewRecord) {
		return caidanService.get(menuCode, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("sys:caidan:view")
	@RequestMapping(value = {"list", ""})
	public String list(Caidan caidan, Model model) {
		model.addAttribute("caidan", caidan);
		return "modules/sys/caidanList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("sys:caidan:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public List<Caidan> listData(Caidan caidan) {
		if (StringUtils.isBlank(caidan.getParentCode())) {
			caidan.setParentCode(Caidan.ROOT_CODE);
		}
		if (StringUtils.isNotBlank(caidan.getMenuName())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getMenuType())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getMenuHref())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getMenuTarget())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getMenuIcon())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getMenuColor())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getPermission())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getIsShow())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getSysCode())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getModuleCodes())){
			caidan.setParentCode(null);
		}
		if (StringUtils.isNotBlank(caidan.getRemarks())){
			caidan.setParentCode(null);
		}
		List<Caidan> list = caidanService.findList(caidan);
		return list;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("sys:caidan:view")
	@RequestMapping(value = "form")
	public String form(Caidan caidan, Model model) {
		// 创建并初始化下一个节点信息
		caidan = createNextNode(caidan);
		model.addAttribute("caidan", caidan);
		return "modules/sys/caidanForm";
	}
	
	/**
	 * 创建并初始化下一个节点信息，如：排序号、默认值
	 */
	@RequiresPermissions("sys:caidan:edit")
	@RequestMapping(value = "createNextNode")
	@ResponseBody
	public Caidan createNextNode(Caidan caidan) {
		if (StringUtils.isNotBlank(caidan.getParentCode())){
			caidan.setParent(caidanService.get(caidan.getParentCode()));
		}
		if (caidan.getIsNewRecord()) {
			Caidan where = new Caidan();
			where.setParentCode(caidan.getParentCode());
			Caidan last = caidanService.getLastByParentCode(where);
			// 获取到下级最后一个节点
			if (last != null){
				caidan.setTreeSort(last.getTreeSort() + 30);
				caidan.setId(IdGen.nextCode(last.getId()));
			}else if (caidan.getParent() != null){
				caidan.setId(caidan.getParent().getId() + "001");
			}
		}
		// 以下设置表单默认数据
		if (caidan.getTreeSort() == null){
			caidan.setTreeSort(Caidan.DEFAULT_TREE_SORT);
		}
		return caidan;
	}

	/**
	 * 保存菜单表
	 */
	@RequiresPermissions("sys:caidan:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Caidan caidan) {
		caidanService.save(caidan);
		return renderResult(Global.TRUE, text("保存菜单表成功！"));
	}
	
	/**
	 * 删除菜单表
	 */
	@RequiresPermissions("sys:caidan:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Caidan caidan) {
		caidanService.delete(caidan);
		return renderResult(Global.TRUE, text("删除菜单表成功！"));
	}
	
	/**
	 * 获取树结构数据
	 * @param excludeCode 排除的Code
	 * @param isShowCode 是否显示编码（true or 1：显示在左侧；2：显示在右侧；false or null：不显示）
	 * @return
	 */
	@RequiresPermissions("sys:caidan:view")
	@RequestMapping(value = "treeData")
	@ResponseBody
	public List<Map<String, Object>> treeData(String excludeCode, String isShowCode) {
		List<Map<String, Object>> mapList = ListUtils.newArrayList();
		List<Caidan> list = caidanService.findList(new Caidan());
		for (int i=0; i<list.size(); i++){
			Caidan e = list.get(i);
			// 过滤非正常的数据
			if (!Caidan.STATUS_NORMAL.equals(e.getStatus())){
				continue;
			}
			// 过滤被排除的编码（包括所有子级）
			if (StringUtils.isNotBlank(excludeCode)){
				if (e.getId().equals(excludeCode)){
					continue;
				}
				if (e.getParentCodes().contains("," + excludeCode + ",")){
					continue;
				}
			}
			Map<String, Object> map = MapUtils.newHashMap();
			map.put("id", e.getId());
			map.put("pId", e.getParentCode());
			map.put("name", StringUtils.getTreeNodeName(isShowCode, e.getId(), e.getId()));
			mapList.add(map);
		}
		return mapList;
	}

	/**
	 * 修复表结构相关数据
	 */
	@RequiresPermissions("sys:caidan:edit")
	@RequestMapping(value = "fixTreeData")
	@ResponseBody
	public String fixTreeData(Caidan caidan){
		if (!UserUtils.getUser().isAdmin()){
			return renderResult(Global.FALSE, "操作失败，只有管理员才能进行修复！");
		}
		caidanService.fixTreeData();
		return renderResult(Global.TRUE, "数据修复成功");
	}
	
	/**
	 * 一级菜单
	 */
	@RequestMapping(value = "findParentName")
	@ResponseBody
	public List<Caidan> findParentName(Caidan caidan) {
		logger.info("find caidan is param:   " + caidan);
		return caidanService.findParentName(caidan);
	}
	
	/**
	 * 二级菜单
	 */
	@RequestMapping(value = "findSecond")
	@ResponseBody
	public List<Caidan> findSecond(String menuCode) {
		logger.info("find caidan is param:   " + menuCode);
		return caidanService.findSecond(menuCode);
	}
	
	/**
	 * 所有菜单
	 */
	@RequestMapping(value = "findAll")
	@ResponseBody
	public List<Caidan> findAll(Caidan caidan) {
		logger.info("find caidan is param:   " + caidan);
		return caidanService.findAll(caidan);
	}
	
	/**
	 * 所有url菜单
	 */
	@RequestMapping(value = "findAllUrl")
	@ResponseBody
	public List<Caidan> findAllUrl(Caidan caidan) {
		logger.info("find caidan is param:   " + caidan);
		return caidanService.findAllUrl(caidan);
	}
}