/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.calendar.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.calendar.entity.CalendarTask;
import com.jeesite.modules.calendar.service.CalendarTaskService;

/**
 * calendar_taskController
 * @author 曾
 * @version 2019-02-13
 */
@Controller
@RequestMapping(value = "${adminPath}/calendar/calendarTask")
public class CalendarTaskController extends BaseController {

	@Autowired
	private CalendarTaskService calendarTaskService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public CalendarTask get(String id, boolean isNewRecord) {
		return calendarTaskService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(CalendarTask calendarTask, Model model) {
		model.addAttribute("calendarTask", calendarTask);
		return "modules/calendar/calendarTaskList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<CalendarTask> listData(CalendarTask calendarTask, HttpServletRequest request, HttpServletResponse response) {
		calendarTask.setPage(new Page<>(request, response));
		Page<CalendarTask> page = calendarTaskService.findPage(calendarTask);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(CalendarTask calendarTask, Model model) {
		model.addAttribute("calendarTask", calendarTask);
		return "modules/calendar/calendarTaskForm";
	}

	/**
	 * 保存calendar_task
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated CalendarTask calendarTask) {
		calendarTaskService.save(calendarTask);
		return renderResult(Global.TRUE, text("保存calendar_task成功！"));
	}
	
	/**
	 * 删除calendar_task
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(CalendarTask calendarTask) {
		calendarTaskService.delete(calendarTask);
		return renderResult(Global.TRUE, text("删除calendar_task成功！"));
	}
	
	@RequestMapping(value = "findTitle")
	@ResponseBody
	public List<CalendarTask> findTitle(CalendarTask calendarTask) {
		logger.info("find caidan is param:   " + calendarTask);
		return calendarTaskService.findAll(calendarTask);
	}
	
}