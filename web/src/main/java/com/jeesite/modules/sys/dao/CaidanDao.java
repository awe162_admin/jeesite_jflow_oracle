/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jeesite.common.dao.TreeDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.sys.entity.Caidan;

/**
 * 菜单表DAO接口
 * @author 曾
 * @version 2019-02-13
 */
@MyBatisDao
public interface CaidanDao extends TreeDao<Caidan> {
	List<Caidan> findParentName(Caidan caidan);
	List<Caidan> findAll(Caidan caidan);
	List<Caidan> findAllUrl(Caidan caidan);
	List<Caidan> findSecond(@Param("menuCode") String menuCode);
}