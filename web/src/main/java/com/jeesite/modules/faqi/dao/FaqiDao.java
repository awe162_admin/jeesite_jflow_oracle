/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faqi.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.faqi.entity.Faqi;

/**
 * 发起DAO接口
 * @author 曾
 * @version 2019-02-18
 */
@MyBatisDao
public interface FaqiDao extends CrudDao<Faqi> {
	
}