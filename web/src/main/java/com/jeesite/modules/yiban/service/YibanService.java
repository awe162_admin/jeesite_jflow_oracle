/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.yiban.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.yiban.entity.Yiban;
import com.jeesite.modules.yiban.dao.YibanDao;

/**
 * 已办Service
 * @author 曾
 * @version 2019-02-18
 */
@Service
@Transactional(readOnly=true)
public class YibanService extends CrudService<YibanDao, Yiban> {
	
	/**
	 * 获取单条数据
	 * @param yiban
	 * @return
	 */
	@Override
	public Yiban get(Yiban yiban) {
		return super.get(yiban);
	}
	
	/**
	 * 查询分页数据
	 * @param yiban 查询条件
	 * @param yiban.page 分页对象
	 * @return
	 */
	@Override
	public Page<Yiban> findPage(Yiban yiban) {
		return super.findPage(yiban);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param yiban
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Yiban yiban) {
		super.save(yiban);
	}
	
	/**
	 * 更新状态
	 * @param yiban
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Yiban yiban) {
		super.updateStatus(yiban);
	}
	
	/**
	 * 删除数据
	 * @param yiban
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Yiban yiban) {
		super.delete(yiban);
	}
	
}