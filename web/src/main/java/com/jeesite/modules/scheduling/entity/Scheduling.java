/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.scheduling.entity;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 班次Entity
 * @author liuchang
 * @version 2019-02-19
 */
@Table(name="scheduling", alias="a", columns={
		@Column(name="code", attrName="code", label="代码"),
		@Column(name="describe", attrName="describe", label="描述"),
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(includeEntity=DataEntity.class),
		@Column(name="department", attrName="department", label="部门"),
		@Column(name="workif", attrName="workif", label="是否参加工作"),
		@Column(name="detaile", attrName="detaile", label="详情描述"),
		@Column(name="starttime", attrName="starttime", label="开始时间"),
		@Column(name="endtime", attrName="endtime", label="结束时间"),
		@Column(name="demand_department", attrName="demandDepartment", label="需求部门"),
		@Column(name="bookmark", attrName="bookmark", label="书签"),
		@Column(name="calendar_picture", attrName="calendarPicture", label="日历配置图片"),
		@Column(name="company", attrName="company", label="公司"),
		@Column(name="name", attrName="name", label="名称"),
	}, orderBy="a.update_date DESC"
)
public class Scheduling extends DataEntity<Scheduling> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 代码
	private String describe;		// 描述
	private String department;		// 部门
	private String workif;		// 是否参加工作
	private String detaile;		// 详情描述
	private Date starttime;		// 开始时间
	private Date endtime;		// 结束时间
	private String demandDepartment;		// 需求部门
	private String bookmark;		// 书签
	private String calendarPicture;		// 日历配置图片
	private String company;		// 公司
	private String name;		// 名称
	
	public Scheduling() {
		this(null);
	}

	public Scheduling(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="代码长度不能超过 255 个字符")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Length(min=0, max=255, message="描述长度不能超过 255 个字符")
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
	@NotBlank(message="部门不能为空")
	@Length(min=0, max=255, message="部门长度不能超过 255 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@NotBlank(message="是否参加工作不能为空")
	@Length(min=0, max=1, message="是否参加工作长度不能超过 1 个字符")
	public String getWorkif() {
		return workif;
	}

	public void setWorkif(String workif) {
		this.workif = workif;
	}
	
	@Length(min=0, max=255, message="详情描述长度不能超过 255 个字符")
	public String getDetaile() {
		return detaile;
	}

	public void setDetaile(String detaile) {
		this.detaile = detaile;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="开始时间不能为空")
	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="结束时间不能为空")
	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	
	@NotBlank(message="需求部门不能为空")
	@Length(min=0, max=255, message="需求部门长度不能超过 255 个字符")
	public String getDemandDepartment() {
		return demandDepartment;
	}

	public void setDemandDepartment(String demandDepartment) {
		this.demandDepartment = demandDepartment;
	}
	
	@Length(min=0, max=255, message="书签长度不能超过 255 个字符")
	public String getBookmark() {
		return bookmark;
	}

	public void setBookmark(String bookmark) {
		this.bookmark = bookmark;
	}
	
	@Length(min=0, max=255, message="日历配置图片长度不能超过 255 个字符")
	public String getCalendarPicture() {
		return calendarPicture;
	}

	public void setCalendarPicture(String calendarPicture) {
		this.calendarPicture = calendarPicture;
	}
	
	@Length(min=0, max=255, message="公司长度不能超过 255 个字符")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@Length(min=0, max=255, message="名称长度不能超过 255 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}