/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.staffadjust.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.staffadjust.entity.Staffadjust;

/**
 * staffadjustDAO接口
 * @author liuchang
 * @version 2019-02-19
 */
@MyBatisDao
public interface StaffadjustDao extends CrudDao<Staffadjust> {
	
}