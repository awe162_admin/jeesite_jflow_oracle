/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faultreport.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.faultreport.entity.Faultreport;
import com.jeesite.modules.faultreport.service.FaultreportService;

/**
 * faultreportController
 * @author liuchang
 * @version 2019-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/faultreport/faultreport")
public class FaultreportController extends BaseController {

	@Autowired
	private FaultreportService faultreportService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Faultreport get(String id, boolean isNewRecord) {
		return faultreportService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("faultreport:faultreport:view")
	@RequestMapping(value = {"list", ""})
	public String list(Faultreport faultreport, Model model) {
		model.addAttribute("faultreport", faultreport);
		return "modules/faultreport/faultreportList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("faultreport:faultreport:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Faultreport> listData(Faultreport faultreport, HttpServletRequest request, HttpServletResponse response) {
		faultreport.setPage(new Page<>(request, response));
		Page<Faultreport> page = faultreportService.findPage(faultreport);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("faultreport:faultreport:view")
	@RequestMapping(value = "form")
	public String form(Faultreport faultreport, Model model) {
		model.addAttribute("faultreport", faultreport);
		return "modules/faultreport/faultreportForm";
	}

	/**
	 * 保存事故调查报告
	 */
	@RequiresPermissions("faultreport:faultreport:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Faultreport faultreport) {
		faultreportService.save(faultreport);
		return renderResult(Global.TRUE, text("保存事故调查报告成功！"));
	}
	
	/**
	 * 删除事故调查报告
	 */
	@RequiresPermissions("faultreport:faultreport:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Faultreport faultreport) {
		faultreportService.delete(faultreport);
		return renderResult(Global.TRUE, text("删除事故调查报告成功！"));
	}
	
}