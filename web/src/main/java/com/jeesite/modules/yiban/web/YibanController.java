/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.yiban.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.yiban.entity.Yiban;
import com.jeesite.modules.yiban.service.YibanService;

/**
 * 已办Controller
 * @author 曾
 * @version 2019-02-18
 */
@Controller
@RequestMapping(value = "${adminPath}/yiban/yiban")
public class YibanController extends BaseController {

	@Autowired
	private YibanService yibanService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Yiban get(String id, boolean isNewRecord) {
		return yibanService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Yiban yiban, Model model) {
		model.addAttribute("yiban", yiban);
		return "modules/yiban/yibanList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Yiban> listData(Yiban yiban, HttpServletRequest request, HttpServletResponse response) {
		yiban.setPage(new Page<>(request, response));
		Page<Yiban> page = yibanService.findPage(yiban);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(Yiban yiban, Model model) {
		model.addAttribute("yiban", yiban);
		return "modules/yiban/yibanForm";
	}

	/**
	 * 保存已办
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Yiban yiban) {
		yibanService.save(yiban);
		return renderResult(Global.TRUE, text("保存已办成功！"));
	}
	
	/**
	 * 删除已办
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Yiban yiban) {
		yibanService.delete(yiban);
		return renderResult(Global.TRUE, text("删除已办成功！"));
	}
	
}