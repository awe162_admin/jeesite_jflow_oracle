/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.calendar.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.calendar.entity.CalendarTask;
import com.jeesite.modules.calendar.dao.CalendarTaskDao;

/**
 * calendar_taskService
 * @author 曾
 * @version 2019-02-13
 */
@Service
@Transactional(readOnly=true)
public class CalendarTaskService extends CrudService<CalendarTaskDao, CalendarTask> {
	
	/**
	 * 获取单条数据
	 * @param calendarTask
	 * @return
	 */
	@Override
	public CalendarTask get(CalendarTask calendarTask) {
		return super.get(calendarTask);
	}
	
	/**
	 * 查询分页数据
	 * @param calendarTask 查询条件
	 * @param calendarTask.page 分页对象
	 * @return
	 */
	@Override
	public Page<CalendarTask> findPage(CalendarTask calendarTask) {
		return super.findPage(calendarTask);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param calendarTask
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(CalendarTask calendarTask) {
		super.save(calendarTask);
	}
	
	/**
	 * 更新状态
	 * @param calendarTask
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(CalendarTask calendarTask) {
		super.updateStatus(calendarTask);
	}
	
	/**
	 * 删除数据
	 * @param calendarTask
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(CalendarTask calendarTask) {
		super.delete(calendarTask);
	}
	
	public List<CalendarTask> findAll(CalendarTask calendarTask) {
		return dao.findTitle(calendarTask);
	}
}