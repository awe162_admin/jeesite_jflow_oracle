/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.arrangeclass.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.arrangeclass.entity.ArrangeClass;
import com.jeesite.modules.arrangeclass.dao.ArrangeClassDao;

/**
 * ArrangeclassService
 * @author liuchang
 * @version 2019-02-19
 */
@Service
@Transactional(readOnly=true)
public class ArrangeClassService extends CrudService<ArrangeClassDao, ArrangeClass> {
	
	/**
	 * 获取单条数据
	 * @param arrangeClass
	 * @return
	 */
	@Override
	public ArrangeClass get(ArrangeClass arrangeClass) {
		return super.get(arrangeClass);
	}
	
	/**
	 * 查询分页数据
	 * @param arrangeClass 查询条件
	 * @param arrangeClass.page 分页对象
	 * @return
	 */
	@Override
	public Page<ArrangeClass> findPage(ArrangeClass arrangeClass) {
		return super.findPage(arrangeClass);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param arrangeClass
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(ArrangeClass arrangeClass) {
		super.save(arrangeClass);
	}
	
	/**
	 * 更新状态
	 * @param arrangeClass
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(ArrangeClass arrangeClass) {
		super.updateStatus(arrangeClass);
	}
	
	/**
	 * 删除数据
	 * @param arrangeClass
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(ArrangeClass arrangeClass) {
		super.delete(arrangeClass);
	}
	
}