/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faultreport.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.faultreport.entity.Faultreport;

/**
 * faultreportDAO接口
 * @author liuchang
 * @version 2019-02-22
 */
@MyBatisDao
public interface FaultreportDao extends CrudDao<Faultreport> {
	
}