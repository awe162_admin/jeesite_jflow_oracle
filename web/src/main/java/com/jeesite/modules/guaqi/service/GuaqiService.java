/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.guaqi.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.guaqi.entity.Guaqi;
import com.jeesite.modules.guaqi.dao.GuaqiDao;

/**
 * 挂起任务Service
 * @author 曾
 * @version 2019-02-18
 */
@Service
@Transactional(readOnly=true)
public class GuaqiService extends CrudService<GuaqiDao, Guaqi> {
	
	/**
	 * 获取单条数据
	 * @param guaqi
	 * @return
	 */
	@Override
	public Guaqi get(Guaqi guaqi) {
		return super.get(guaqi);
	}
	
	/**
	 * 查询分页数据
	 * @param guaqi 查询条件
	 * @param guaqi.page 分页对象
	 * @return
	 */
	@Override
	public Page<Guaqi> findPage(Guaqi guaqi) {
		return super.findPage(guaqi);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param guaqi
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Guaqi guaqi) {
		super.save(guaqi);
	}
	
	/**
	 * 更新状态
	 * @param guaqi
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Guaqi guaqi) {
		super.updateStatus(guaqi);
	}
	
	/**
	 * 删除数据
	 * @param guaqi
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Guaqi guaqi) {
		super.delete(guaqi);
	}
	
}