/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faultreport.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.faultreport.entity.Faultreport;
import com.jeesite.modules.faultreport.dao.FaultreportDao;

/**
 * faultreportService
 * @author liuchang
 * @version 2019-02-22
 */
@Service
@Transactional(readOnly=true)
public class FaultreportService extends CrudService<FaultreportDao, Faultreport> {
	
	/**
	 * 获取单条数据
	 * @param faultreport
	 * @return
	 */
	@Override
	public Faultreport get(Faultreport faultreport) {
		return super.get(faultreport);
	}
	
	/**
	 * 查询分页数据
	 * @param faultreport 查询条件
	 * @param faultreport.page 分页对象
	 * @return
	 */
	@Override
	public Page<Faultreport> findPage(Faultreport faultreport) {
		return super.findPage(faultreport);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param faultreport
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Faultreport faultreport) {
		super.save(faultreport);
	}
	
	/**
	 * 更新状态
	 * @param faultreport
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Faultreport faultreport) {
		super.updateStatus(faultreport);
	}
	
	/**
	 * 删除数据
	 * @param faultreport
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Faultreport faultreport) {
		super.delete(faultreport);
	}
	
}