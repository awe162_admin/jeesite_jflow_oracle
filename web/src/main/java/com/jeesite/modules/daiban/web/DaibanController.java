/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.daiban.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.daiban.entity.Daiban;
import com.jeesite.modules.daiban.service.DaibanService;

/**
 * 代办Controller
 * @author 曾
 * @version 2019-02-18
 */
@Controller
@RequestMapping(value = "${adminPath}/daiban/daiban")
public class DaibanController extends BaseController {

	@Autowired
	private DaibanService daibanService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Daiban get(String id, boolean isNewRecord) {
		return daibanService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Daiban daiban, Model model) {
		model.addAttribute("daiban", daiban);
		return "modules/daiban/daibanList";   
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Daiban> listData(Daiban daiban, HttpServletRequest request, HttpServletResponse response) {
		daiban.setPage(new Page<>(request, response));
		daiban.setPageSize(10);
		Page<Daiban> page = daibanService.findPage(daiban);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(Daiban daiban, Model model) {
		model.addAttribute("daiban", daiban);
		return "modules/daiban/daibanForm";
	}

	/**
	 * 保存代办
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Daiban daiban) {
		daibanService.save(daiban);
		return renderResult(Global.TRUE, text("保存代办成功！"));
	}
	
	/**
	 * 删除代办
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Daiban daiban) {
		daibanService.delete(daiban);
		return renderResult(Global.TRUE, text("删除代办成功！"));
	}
	
}