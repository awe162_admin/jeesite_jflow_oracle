/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.staffadjust.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * staffadjustEntity
 * @author liuchang
 * @version 2019-02-19
 */
@Table(name="staffadjust", alias="a", columns={
		@Column(name="adjustman", attrName="adjustman", label="调整人"),
		@Column(name="bookmark", attrName="bookmark", label="书签"),
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(includeEntity=DataEntity.class),
		@Column(name="operationtime", attrName="operationtime", label="操作时间"),
	}, orderBy="a.update_date DESC"
)
public class Staffadjust extends DataEntity<Staffadjust> {
	
	private static final long serialVersionUID = 1L;
	private String adjustman;		// 调整人
	private String bookmark;		// 书签
	private Date operationtime;		// 操作时间
	
	public Staffadjust() {
		this(null);
	}

	public Staffadjust(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="调整人长度不能超过 255 个字符")
	public String getAdjustman() {
		return adjustman;
	}

	public void setAdjustman(String adjustman) {
		this.adjustman = adjustman;
	}
	
	@Length(min=0, max=255, message="书签长度不能超过 255 个字符")
	public String getBookmark() {
		return bookmark;
	}

	public void setBookmark(String bookmark) {
		this.bookmark = bookmark;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getOperationtime() {
		return operationtime;
	}

	public void setOperationtime(Date operationtime) {
		this.operationtime = operationtime;
	}
	
}