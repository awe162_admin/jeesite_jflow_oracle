/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.staffadjust.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.staffadjust.entity.Staffadjust;
import com.jeesite.modules.staffadjust.dao.StaffadjustDao;

/**
 * staffadjustService
 * @author liuchang
 * @version 2019-02-19
 */
@Service
@Transactional(readOnly=true)
public class StaffadjustService extends CrudService<StaffadjustDao, Staffadjust> {
	
	/**
	 * 获取单条数据
	 * @param staffadjust
	 * @return
	 */
	@Override
	public Staffadjust get(Staffadjust staffadjust) {
		return super.get(staffadjust);
	}
	
	/**
	 * 查询分页数据
	 * @param staffadjust 查询条件
	 * @param staffadjust.page 分页对象
	 * @return
	 */
	@Override
	public Page<Staffadjust> findPage(Staffadjust staffadjust) {
		return super.findPage(staffadjust);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param staffadjust
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Staffadjust staffadjust) {
		super.save(staffadjust);
	}
	
	/**
	 * 更新状态
	 * @param staffadjust
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Staffadjust staffadjust) {
		super.updateStatus(staffadjust);
	}
	
	/**
	 * 删除数据
	 * @param staffadjust
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Staffadjust staffadjust) {
		super.delete(staffadjust);
	}
	
}