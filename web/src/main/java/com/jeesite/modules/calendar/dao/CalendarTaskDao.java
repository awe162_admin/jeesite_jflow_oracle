/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.calendar.dao;

import java.util.List;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.calendar.entity.CalendarTask;

/**
 * calendar_taskDAO接口
 * @author 曾
 * @version 2019-02-13
 */
@MyBatisDao
public interface CalendarTaskDao extends CrudDao<CalendarTask> {
	
	List<CalendarTask> findTitle(CalendarTask calendarTask);
	
}