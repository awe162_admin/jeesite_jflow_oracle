/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.scheduling.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.scheduling.entity.Scheduling;
import com.jeesite.modules.scheduling.dao.SchedulingDao;

/**
 * 班次Service
 * @author liuchang
 * @version 2019-02-19
 */
@Service
@Transactional(readOnly=true)
public class SchedulingService extends CrudService<SchedulingDao, Scheduling> {
	
	/**
	 * 获取单条数据
	 * @param scheduling
	 * @return
	 */
	@Override
	public Scheduling get(Scheduling scheduling) {
		return super.get(scheduling);
	}
	
	/**
	 * 查询分页数据
	 * @param scheduling 查询条件
	 * @param scheduling.page 分页对象
	 * @return
	 */
	@Override
	public Page<Scheduling> findPage(Scheduling scheduling) {
		return super.findPage(scheduling);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param scheduling
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Scheduling scheduling) {
		super.save(scheduling);
	}
	
	/**
	 * 更新状态
	 * @param scheduling
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Scheduling scheduling) {
		super.updateStatus(scheduling);
	}
	
	/**
	 * 删除数据
	 * @param scheduling
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Scheduling scheduling) {
		super.delete(scheduling);
	}
	
}