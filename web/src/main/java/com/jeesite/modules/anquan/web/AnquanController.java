/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.anquan.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.anquan.entity.Anquan;
import com.jeesite.modules.anquan.service.AnquanService;

/**
 * anquanController
 * @author zz
 * @version 2019-02-12
 */
@Controller
@RequestMapping(value = "${adminPath}/anquan/anquan")
public class AnquanController extends BaseController {

	@Autowired
	private AnquanService anquanService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Anquan get(String id, boolean isNewRecord) {
		return anquanService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("anquan:anquan:view")
	@RequestMapping(value = {"list", ""})
	public String list(Anquan anquan, Model model) {
		model.addAttribute("anquan", anquan);
		return "modules/anquan/anquanList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("anquan:anquan:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Anquan> listData(Anquan anquan, HttpServletRequest request, HttpServletResponse response) {
		anquan.setPage(new Page<>(request, response));
		Page<Anquan> page = anquanService.findPage(anquan);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("anquan:anquan:view")
	@RequestMapping(value = "form")
	public String form(Anquan anquan, Model model) {
		model.addAttribute("anquan", anquan);
		return "modules/anquan/anquanForm";
	}

	/**
	 * 保存安全事件通报
	 */
	@RequiresPermissions("anquan:anquan:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Anquan anquan) {
		anquanService.save(anquan);
		return renderResult(Global.TRUE, text("保存安全事件通报成功！"));
	}
	
	/**
	 * 删除安全事件通报
	 */
	@RequiresPermissions("anquan:anquan:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Anquan anquan) {
		anquanService.delete(anquan);
		return renderResult(Global.TRUE, text("删除安全事件通报成功！"));
	}
	
}