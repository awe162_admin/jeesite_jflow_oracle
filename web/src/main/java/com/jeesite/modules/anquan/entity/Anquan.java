/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.anquan.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * anquanEntity
 * @author zz
 * @version 2019-02-12
 */
@Table(name="anquan", alias="a", columns={
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(name="shuqian", attrName="shuqian", label="书签"),
		@Column(name="company", attrName="company", label="公司"),
		@Column(includeEntity=DataEntity.class),
		@Column(name="bianhao", attrName="bianhao", label="需求计划编号"),
		@Column(name="miaoshu", attrName="miaoshu", label="需求描述"),
		@Column(name="mingcheng", attrName="mingcheng", label="项目名称"),
		@Column(name="leixing", attrName="leixing", label="计划类型"),
		@Column(name="bumen", attrName="bumen", label="需求部门"),
		@Column(name="zuzhi", attrName="zuzhi", label="采购组织"),
		@Column(name="shenqingren", attrName="shenqingren", label="申请人"),
		@Column(name="shenqingjine", attrName="shenqingjine", label="申请金额"),
		@Column(name="shenhejine", attrName="shenhejine", label="审核金额"),
		@Column(name="shenheren", attrName="shenheren", label="当前审核人"),
		@Column(name="xuqiuleixing", attrName="xuqiuleixing", label="需求类型"),
		@Column(name="shenqingriqi", attrName="shenqingriqi", label="申请日期"),
		@Column(name="shenqingyongtu", attrName="shenqingyongtu", label="申请用途"),
	}, orderBy="a.update_date DESC"
)
public class Anquan extends DataEntity<Anquan> {
	
	private static final long serialVersionUID = 1L;
	private String shuqian;		// 书签
	private String company;		// 公司
	private String bianhao;		// 需求计划编号
	private String miaoshu;		// 需求描述
	private String mingcheng;		// 项目名称
	private String leixing;		// 计划类型
	private String bumen;		// 需求部门
	private String zuzhi;		// 采购组织
	private String shenqingren;		// 申请人
	private String shenqingjine;		// 申请金额
	private String shenhejine;		// 审核金额
	private String shenheren;		// 当前审核人
	private String xuqiuleixing;		// 需求类型
	private Date shenqingriqi;		// 申请日期
	private String shenqingyongtu;		// 申请用途
	
	public Anquan() {
		this(null);
	}

	public Anquan(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="书签长度不能超过 255 个字符")
	public String getShuqian() {
		return shuqian;
	}

	public void setShuqian(String shuqian) {
		this.shuqian = shuqian;
	}
	
	@Length(min=0, max=255, message="公司长度不能超过 255 个字符")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@Length(min=0, max=255, message="需求计划编号长度不能超过 255 个字符")
	public String getBianhao() {
		return bianhao;
	}

	public void setBianhao(String bianhao) {
		this.bianhao = bianhao;
	}
	
	@Length(min=0, max=255, message="需求描述长度不能超过 255 个字符")
	public String getMiaoshu() {
		return miaoshu;
	}

	public void setMiaoshu(String miaoshu) {
		this.miaoshu = miaoshu;
	}
	
	@Length(min=0, max=255, message="项目名称长度不能超过 255 个字符")
	public String getMingcheng() {
		return mingcheng;
	}

	public void setMingcheng(String mingcheng) {
		this.mingcheng = mingcheng;
	}
	
	@Length(min=0, max=255, message="计划类型长度不能超过 255 个字符")
	public String getLeixing() {
		return leixing;
	}

	public void setLeixing(String leixing) {
		this.leixing = leixing;
	}
	
	@Length(min=0, max=255, message="需求部门长度不能超过 255 个字符")
	public String getBumen() {
		return bumen;
	}

	public void setBumen(String bumen) {
		this.bumen = bumen;
	}
	
	@Length(min=0, max=255, message="采购组织长度不能超过 255 个字符")
	public String getZuzhi() {
		return zuzhi;
	}

	public void setZuzhi(String zuzhi) {
		this.zuzhi = zuzhi;
	}
	
	@Length(min=0, max=255, message="申请人长度不能超过 255 个字符")
	public String getShenqingren() {
		return shenqingren;
	}

	public void setShenqingren(String shenqingren) {
		this.shenqingren = shenqingren;
	}
	
	@Length(min=0, max=255, message="申请金额长度不能超过 255 个字符")
	public String getShenqingjine() {
		return shenqingjine;
	}

	public void setShenqingjine(String shenqingjine) {
		this.shenqingjine = shenqingjine;
	}
	
	@Length(min=0, max=255, message="审核金额长度不能超过 255 个字符")
	public String getShenhejine() {
		return shenhejine;
	}

	public void setShenhejine(String shenhejine) {
		this.shenhejine = shenhejine;
	}
	
	@Length(min=0, max=255, message="当前审核人长度不能超过 255 个字符")
	public String getShenheren() {
		return shenheren;
	}

	public void setShenheren(String shenheren) {
		this.shenheren = shenheren;
	}
	
	@Length(min=0, max=255, message="需求类型长度不能超过 255 个字符")
	public String getXuqiuleixing() {
		return xuqiuleixing;
	}

	public void setXuqiuleixing(String xuqiuleixing) {
		this.xuqiuleixing = xuqiuleixing;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getShenqingriqi() {
		return shenqingriqi;
	}

	public void setShenqingriqi(Date shenqingriqi) {
		this.shenqingriqi = shenqingriqi;
	}
	
	@Length(min=0, max=255, message="申请用途长度不能超过 255 个字符")
	public String getShenqingyongtu() {
		return shenqingyongtu;
	}

	public void setShenqingyongtu(String shenqingyongtu) {
		this.shenqingyongtu = shenqingyongtu;
	}
	
}