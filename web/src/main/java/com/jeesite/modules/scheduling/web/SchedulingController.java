/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.scheduling.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.scheduling.entity.Scheduling;
import com.jeesite.modules.scheduling.service.SchedulingService;

/**
 * 班次Controller
 * @author liuchang
 * @version 2019-02-19
 */
@Controller
@RequestMapping(value = "${adminPath}/scheduling/scheduling")
public class SchedulingController extends BaseController {

	@Autowired
	private SchedulingService schedulingService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Scheduling get(String id, boolean isNewRecord) {
		return schedulingService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("scheduling:scheduling:view")
	@RequestMapping(value = {"list", ""})
	public String list(Scheduling scheduling, Model model) {
		model.addAttribute("scheduling", scheduling);
		return "modules/scheduling/schedulingList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("scheduling:scheduling:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Scheduling> listData(Scheduling scheduling, HttpServletRequest request, HttpServletResponse response) {
		scheduling.setPage(new Page<>(request, response));
		Page<Scheduling> page = schedulingService.findPage(scheduling);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("scheduling:scheduling:view")
	@RequestMapping(value = "form")
	public String form(Scheduling scheduling, Model model) {
		model.addAttribute("scheduling", scheduling);
		return "modules/scheduling/schedulingForm";
	}

	/**
	 * 保存班次
	 */
	@RequiresPermissions("scheduling:scheduling:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Scheduling scheduling) {
		schedulingService.save(scheduling);
		return renderResult(Global.TRUE, text("保存班次成功！"));
	}
	
	/**
	 * 删除班次
	 */
	@RequiresPermissions("scheduling:scheduling:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Scheduling scheduling) {
		schedulingService.delete(scheduling);
		return renderResult(Global.TRUE, text("删除班次成功！"));
	}
	
}