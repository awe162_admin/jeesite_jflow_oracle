/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.arrangeclass.entity;

import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * ArrangeclassEntity
 * @author liuchang
 * @version 2019-02-19
 */
@Table(name="class", alias="a", columns={
		@Column(name="company", attrName="company", label="公司"),
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(includeEntity=DataEntity.class),
		@Column(name="department", attrName="department", label="部门"),
		@Column(name="starttime", attrName="starttime", label="开始时间"),
		@Column(name="endtime", attrName="endtime", label="结束时间"),
		@Column(name="cycle", attrName="cycle", label="周期"),
	}, orderBy="a.update_date DESC"
)
public class ArrangeClass extends DataEntity<ArrangeClass> {
	
	private static final long serialVersionUID = 1L;
	private String company;		// 公司
	private String department;		// 部门
	private Date starttime;		// 开始时间
	private Date endtime;		// 结束时间
	private String cycle;		// 周期
	
	public ArrangeClass() {
		this(null);
	}

	public ArrangeClass(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="公司长度不能超过 255 个字符")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@NotBlank(message="部门不能为空")
	@Length(min=0, max=255, message="部门长度不能超过 255 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="开始时间不能为空")
	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="结束时间不能为空")
	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	
	@NotBlank(message="周期不能为空")
	@Length(min=0, max=4, message="周期长度不能超过 4 个字符")
	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	
}