# jeesite_jflow_mysql

#### 介绍
参照网上 ThinkGem大神的开源作品和著名jflow，由于Think集成的部分开源，本人觉得严重影响了使用风格。所以集成此版，jeesite4.x和jflow的集成。
本人QQ:553130993

#### 软件架构
软件架构说明
请查看jeesite4.x  和Jflow说明文档

#### 安装教程

请查看jeesite4.x  和Jflow说明文档

#### 使用说明

请查看jeesite4.x  和Jflow说明文档

#### 参与贡献

请查看jeesite4.x  和Jflow说明文档


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)