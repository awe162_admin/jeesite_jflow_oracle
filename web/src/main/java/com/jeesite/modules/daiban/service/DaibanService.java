/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.daiban.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.daiban.entity.Daiban;
import com.jeesite.modules.daiban.dao.DaibanDao;

/**
 * 代办Service
 * @author 曾
 * @version 2019-02-18
 */
@Service
@Transactional(readOnly=true)
public class DaibanService extends CrudService<DaibanDao, Daiban> {
	
	/**
	 * 获取单条数据
	 * @param daiban
	 * @return
	 */
	@Override
	public Daiban get(Daiban daiban) {
		return super.get(daiban);
	}
	
	/**
	 * 查询分页数据
	 * @param daiban 查询条件
	 * @param daiban.page 分页对象
	 * @return
	 */
	@Override
	public Page<Daiban> findPage(Daiban daiban) {
		return super.findPage(daiban);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param daiban
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Daiban daiban) {
		super.save(daiban);
	}
	
	/**
	 * 更新状态
	 * @param daiban
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Daiban daiban) {
		super.updateStatus(daiban);
	}
	
	/**
	 * 删除数据
	 * @param daiban
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Daiban daiban) {
		super.delete(daiban);
	}
	
}