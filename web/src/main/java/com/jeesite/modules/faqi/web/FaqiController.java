/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faqi.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.faqi.entity.Faqi;
import com.jeesite.modules.faqi.service.FaqiService;

/**
 * 发起Controller
 * @author 曾
 * @version 2019-02-18
 */
@Controller
@RequestMapping(value = "${adminPath}/faqi/faqi")
public class FaqiController extends BaseController {

	@Autowired
	private FaqiService faqiService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Faqi get(String id, boolean isNewRecord) {
		return faqiService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Faqi faqi, Model model) {
		model.addAttribute("faqi", faqi);
		return "modules/faqi/faqiList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Faqi> listData(Faqi faqi, HttpServletRequest request, HttpServletResponse response) {
		faqi.setPage(new Page<>(request, response));
		faqi.setPageSize(10);
		Page<Faqi> page = faqiService.findPage(faqi);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(Faqi faqi, Model model) {
		model.addAttribute("faqi", faqi);
		return "modules/faqi/faqiForm";
	}

	/**
	 * 保存发起
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Faqi faqi) {
		faqiService.save(faqi);
		return renderResult(Global.TRUE, text("保存发起成功！"));
	}
	
	/**
	 * 删除发起
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Faqi faqi) {
		faqiService.delete(faqi);
		return renderResult(Global.TRUE, text("删除发起成功！"));
	}
	
}