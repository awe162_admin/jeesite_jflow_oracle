/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.guaqi.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.guaqi.entity.Guaqi;
import com.jeesite.modules.guaqi.service.GuaqiService;

/**
 * 挂起任务Controller
 * @author 曾
 * @version 2019-02-18
 */
@Controller
@RequestMapping(value = "${adminPath}/guaqi/guaqi")
public class GuaqiController extends BaseController {

	@Autowired
	private GuaqiService guaqiService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Guaqi get(String id, boolean isNewRecord) {
		return guaqiService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Guaqi guaqi, Model model) {
		model.addAttribute("guaqi", guaqi);
		return "modules/guaqi/guaqiList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Guaqi> listData(Guaqi guaqi, HttpServletRequest request, HttpServletResponse response) {
		guaqi.setPage(new Page<>(request, response));
		guaqi.setPageSize(10);
		Page<Guaqi> page = guaqiService.findPage(guaqi);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequestMapping(value = "form")
	public String form(Guaqi guaqi, Model model) {
		model.addAttribute("guaqi", guaqi);
		return "modules/guaqi/guaqiForm";
	}

	/**
	 * 保存挂起任务
	 */
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Guaqi guaqi) {
		guaqiService.save(guaqi);
		return renderResult(Global.TRUE, text("保存挂起任务成功！"));
	}
	
	/**
	 * 删除挂起任务
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Guaqi guaqi) {
		guaqiService.delete(guaqi);
		return renderResult(Global.TRUE, text("删除挂起任务成功！"));
	}
	
}