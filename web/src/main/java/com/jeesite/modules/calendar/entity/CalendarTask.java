/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.calendar.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * calendar_taskEntity
 * @author 曾
 * @version 2019-02-13
 */
@Table(name="calendar_task", alias="a", columns={
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(name="title", attrName="title", label="标题", queryType=QueryType.LIKE),
		@Column(includeEntity=DataEntity.class),
		@Column(name="enddate", attrName="enddate", label="结束时间"),
		@Column(name="type", attrName="type", label="类型"),
		@Column(name="remind", attrName="remind", label="提醒"),
		@Column(name="team", attrName="team", label="所属组"),
		@Column(name="message", attrName="message", label="消息提醒"),
	}, orderBy="a.update_date DESC"
)
public class CalendarTask extends DataEntity<CalendarTask> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 标题
	private Date enddate;		// 结束时间
	private String type;		// 类型
	private String remind;		// 提醒
	private String team;		// 所属组
	private String message;		// 消息提醒
	
	public CalendarTask() {
		this(null);
	}

	public CalendarTask(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="标题长度不能超过 255 个字符")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	
	@Length(min=0, max=255, message="类型长度不能超过 255 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=255, message="提醒长度不能超过 255 个字符")
	public String getRemind() {
		return remind;
	}

	public void setRemind(String remind) {
		this.remind = remind;
	}
	
	@Length(min=0, max=255, message="所属组长度不能超过 255 个字符")
	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	@Length(min=0, max=255, message="消息提醒长度不能超过 255 个字符")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}