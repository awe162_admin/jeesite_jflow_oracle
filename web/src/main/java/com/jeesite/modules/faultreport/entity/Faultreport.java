/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.faultreport.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * faultreportEntity
 * @author liuchang
 * @version 2019-02-22
 */
@Table(name="faultreport", alias="a", columns={
		@Column(name="code", attrName="code", label="代码"),
		@Column(name="describe", attrName="describe", label="描述"),
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(includeEntity=DataEntity.class),
		@Column(name="department", attrName="department", label="部门"),
		@Column(name="workif", attrName="workif", label="是否参加工作"),
		@Column(name="detaile", attrName="detaile", label="详情描述"),
		@Column(name="starttime", attrName="starttime", label="开始时间"),
		@Column(name="endtime", attrName="endtime", label="结束时间"),
		@Column(name="bookmark", attrName="bookmark", label="书签"),
		@Column(name="demand_department", attrName="demandDepartment", label="需求部门"),
		@Column(name="calendar_picture", attrName="calendarPicture", label="日历配置图片"),
		@Column(name="company", attrName="company", label="公司"),
		@Column(name="demandcode", attrName="demandcode", label="需求计划编号"),
		@Column(name="projectname", attrName="projectname", label="项目名称"),
		@Column(name="plantype", attrName="plantype", label="计划类型"),
		@Column(name="purchaseorg", attrName="purchaseorg", label="采购组织"),
		@Column(name="applyman", attrName="applyman", label="申请人"),
		@Column(name="applymoney", attrName="applymoney", label="申请金额"),
		@Column(name="examinemoney", attrName="examinemoney", label="审核金额"),
		@Column(name="nowexamineman", attrName="nowexamineman", label="当前审核人"),
		@Column(name="demandtype", attrName="demandtype", label="需求类型"),
		@Column(name="applydate", attrName="applydate", label="申请日期"),
		@Column(name="applypurpose", attrName="applypurpose", label="申请用途"),
		@Column(name="investigationrepcode", attrName="investigationrepcode", label="调查报告编号"),
		@Column(name="name", attrName="name", label="名称"),
		@Column(name="address", attrName="address", label="地点"),
		@Column(name="type", attrName="type", label="类型"),
		@Column(name="level", attrName="level", label="级别"),
		@Column(name="reason", attrName="reason", label="原因"),
		@Column(name="directecoloss", attrName="directecoloss", label="直接经济损失"),
		@Column(name="seriouslnjurynumber", attrName="seriouslnjurynumber", label="重伤人数"),
		@Column(name="minorwoundnumber", attrName="minorwoundnumber", label="轻伤人数"),
		@Column(name="category", attrName="category", label="类别"),
		@Column(name="nature", attrName="nature", label="性质"),
		@Column(name="lossnumber", attrName="lossnumber", label="损工人数"),
		@Column(name="correspondingrep", attrName="correspondingrep", label="对应快报"),
		@Column(name="ifoutage", attrName="ifoutage", label="是否计划停运"),
		@Column(name="reasontype", attrName="reasontype", label="原因类别"),
		@Column(name="deadnumber", attrName="deadnumber", label="死亡人数"),
		@Column(name="limitnumber", attrName="limitnumber", label="限工人数"),
		@Column(name="indirectecoloss", attrName="indirectecoloss", label="间接经济损失"),
		@Column(name="time", attrName="time", label="时间"),
		@Column(name="demanddescribe", attrName="demanddescribe", label="需求描述"),
	}, orderBy="a.update_date DESC"
)
public class Faultreport extends DataEntity<Faultreport> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 代码
	private String describe;		// 描述
	private String department;		// 部门
	private String workif;		// 是否参加工作
	private String detaile;		// 详情描述
	private Date starttime;		// 开始时间
	private Date endtime;		// 结束时间
	private String bookmark;		// 书签
	private String demandDepartment;		// 需求部门
	private String calendarPicture;		// 日历配置图片
	private String company;		// 公司
	private String demandcode;		// 需求计划编号
	private String projectname;		// 项目名称
	private String plantype;		// 计划类型
	private String purchaseorg;		// 采购组织
	private String applyman;		// 申请人
	private String applymoney;		// 申请金额
	private String examinemoney;		// 审核金额
	private String nowexamineman;		// 当前审核人
	private String demandtype;		// 需求类型
	private Date applydate;		// 申请日期
	private String applypurpose;		// 申请用途
	private String investigationrepcode;		// 调查报告编号
	private String name;		// 名称
	private String address;		// 地点
	private String type;		// 类型
	private String level;		// 级别
	private String reason;		// 原因
	private String directecoloss;		// 直接经济损失
	private String seriouslnjurynumber;		// 重伤人数
	private String minorwoundnumber;		// 轻伤人数
	private String category;		// 类别
	private String nature;		// 性质
	private String lossnumber;		// 损工人数
	private String correspondingrep;		// 对应快报
	private String ifoutage;		// 是否计划停运
	private String reasontype;		// 原因类别
	private String deadnumber;		// 死亡人数
	private String limitnumber;		// 限工人数
	private String indirectecoloss;		// 间接经济损失
	private Date time;		// 时间
	private String demanddescribe;		// 需求描述
	
	public Faultreport() {
		this(null);
	}

	public Faultreport(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="代码长度不能超过 255 个字符")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Length(min=0, max=255, message="描述长度不能超过 255 个字符")
	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}
	
	@Length(min=0, max=255, message="部门长度不能超过 255 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Length(min=0, max=1, message="是否参加工作长度不能超过 1 个字符")
	public String getWorkif() {
		return workif;
	}

	public void setWorkif(String workif) {
		this.workif = workif;
	}
	
	@Length(min=0, max=255, message="详情描述长度不能超过 255 个字符")
	public String getDetaile() {
		return detaile;
	}

	public void setDetaile(String detaile) {
		this.detaile = detaile;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	
	@Length(min=0, max=255, message="书签长度不能超过 255 个字符")
	public String getBookmark() {
		return bookmark;
	}

	public void setBookmark(String bookmark) {
		this.bookmark = bookmark;
	}
	
	@Length(min=0, max=255, message="需求部门长度不能超过 255 个字符")
	public String getDemandDepartment() {
		return demandDepartment;
	}

	public void setDemandDepartment(String demandDepartment) {
		this.demandDepartment = demandDepartment;
	}
	
	@Length(min=0, max=255, message="日历配置图片长度不能超过 255 个字符")
	public String getCalendarPicture() {
		return calendarPicture;
	}

	public void setCalendarPicture(String calendarPicture) {
		this.calendarPicture = calendarPicture;
	}
	
	@Length(min=0, max=255, message="公司长度不能超过 255 个字符")
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
	@Length(min=0, max=255, message="需求计划编号长度不能超过 255 个字符")
	public String getDemandcode() {
		return demandcode;
	}

	public void setDemandcode(String demandcode) {
		this.demandcode = demandcode;
	}
	
	@Length(min=0, max=255, message="项目名称长度不能超过 255 个字符")
	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	
	@Length(min=0, max=255, message="计划类型长度不能超过 255 个字符")
	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}
	
	@Length(min=0, max=255, message="采购组织长度不能超过 255 个字符")
	public String getPurchaseorg() {
		return purchaseorg;
	}

	public void setPurchaseorg(String purchaseorg) {
		this.purchaseorg = purchaseorg;
	}
	
	@Length(min=0, max=255, message="申请人长度不能超过 255 个字符")
	public String getApplyman() {
		return applyman;
	}

	public void setApplyman(String applyman) {
		this.applyman = applyman;
	}
	
	@Length(min=0, max=255, message="申请金额长度不能超过 255 个字符")
	public String getApplymoney() {
		return applymoney;
	}

	public void setApplymoney(String applymoney) {
		this.applymoney = applymoney;
	}
	
	@Length(min=0, max=255, message="审核金额长度不能超过 255 个字符")
	public String getExaminemoney() {
		return examinemoney;
	}

	public void setExaminemoney(String examinemoney) {
		this.examinemoney = examinemoney;
	}
	
	@Length(min=0, max=255, message="当前审核人长度不能超过 255 个字符")
	public String getNowexamineman() {
		return nowexamineman;
	}

	public void setNowexamineman(String nowexamineman) {
		this.nowexamineman = nowexamineman;
	}
	
	@Length(min=0, max=255, message="需求类型长度不能超过 255 个字符")
	public String getDemandtype() {
		return demandtype;
	}

	public void setDemandtype(String demandtype) {
		this.demandtype = demandtype;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getApplydate() {
		return applydate;
	}

	public void setApplydate(Date applydate) {
		this.applydate = applydate;
	}
	
	@Length(min=0, max=255, message="申请用途长度不能超过 255 个字符")
	public String getApplypurpose() {
		return applypurpose;
	}

	public void setApplypurpose(String applypurpose) {
		this.applypurpose = applypurpose;
	}
	
	@Length(min=0, max=255, message="调查报告编号长度不能超过 255 个字符")
	public String getInvestigationrepcode() {
		return investigationrepcode;
	}

	public void setInvestigationrepcode(String investigationrepcode) {
		this.investigationrepcode = investigationrepcode;
	}
	
	@NotBlank(message="名称不能为空")
	@Length(min=0, max=255, message="名称长度不能超过 255 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@NotBlank(message="地点不能为空")
	@Length(min=0, max=255, message="地点长度不能超过 255 个字符")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@NotBlank(message="类型不能为空")
	@Length(min=0, max=255, message="类型长度不能超过 255 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@NotBlank(message="级别不能为空")
	@Length(min=0, max=255, message="级别长度不能超过 255 个字符")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	@NotBlank(message="原因不能为空")
	@Length(min=0, max=255, message="原因长度不能超过 255 个字符")
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@NotBlank(message="直接经济损失不能为空")
	@Length(min=0, max=255, message="直接经济损失长度不能超过 255 个字符")
	public String getDirectecoloss() {
		return directecoloss;
	}

	public void setDirectecoloss(String directecoloss) {
		this.directecoloss = directecoloss;
	}
	
	@NotBlank(message="重伤人数不能为空")
	@Length(min=0, max=255, message="重伤人数长度不能超过 255 个字符")
	public String getSeriouslnjurynumber() {
		return seriouslnjurynumber;
	}

	public void setSeriouslnjurynumber(String seriouslnjurynumber) {
		this.seriouslnjurynumber = seriouslnjurynumber;
	}
	
	@NotBlank(message="轻伤人数不能为空")
	@Length(min=0, max=255, message="轻伤人数长度不能超过 255 个字符")
	public String getMinorwoundnumber() {
		return minorwoundnumber;
	}

	public void setMinorwoundnumber(String minorwoundnumber) {
		this.minorwoundnumber = minorwoundnumber;
	}
	
	@NotBlank(message="类别不能为空")
	@Length(min=0, max=255, message="类别长度不能超过 255 个字符")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@NotBlank(message="性质不能为空")
	@Length(min=0, max=255, message="性质长度不能超过 255 个字符")
	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}
	
	@NotBlank(message="损工人数不能为空")
	@Length(min=0, max=255, message="损工人数长度不能超过 255 个字符")
	public String getLossnumber() {
		return lossnumber;
	}

	public void setLossnumber(String lossnumber) {
		this.lossnumber = lossnumber;
	}
	
	@Length(min=0, max=255, message="对应快报长度不能超过 255 个字符")
	public String getCorrespondingrep() {
		return correspondingrep;
	}

	public void setCorrespondingrep(String correspondingrep) {
		this.correspondingrep = correspondingrep;
	}
	
	@Length(min=0, max=255, message="是否计划停运长度不能超过 255 个字符")
	public String getIfoutage() {
		return ifoutage;
	}

	public void setIfoutage(String ifoutage) {
		this.ifoutage = ifoutage;
	}
	
	@Length(min=0, max=255, message="原因类别长度不能超过 255 个字符")
	public String getReasontype() {
		return reasontype;
	}

	public void setReasontype(String reasontype) {
		this.reasontype = reasontype;
	}
	
	@NotBlank(message="死亡人数不能为空")
	@Length(min=0, max=255, message="死亡人数长度不能超过 255 个字符")
	public String getDeadnumber() {
		return deadnumber;
	}

	public void setDeadnumber(String deadnumber) {
		this.deadnumber = deadnumber;
	}
	
	@NotBlank(message="限工人数不能为空")
	@Length(min=0, max=60, message="限工人数长度不能超过 60 个字符")
	public String getLimitnumber() {
		return limitnumber;
	}

	public void setLimitnumber(String limitnumber) {
		this.limitnumber = limitnumber;
	}
	
	@NotBlank(message="间接经济损失不能为空")
	@Length(min=0, max=255, message="间接经济损失长度不能超过 255 个字符")
	public String getIndirectecoloss() {
		return indirectecoloss;
	}

	public void setIndirectecoloss(String indirectecoloss) {
		this.indirectecoloss = indirectecoloss;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="时间不能为空")
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	@Length(min=0, max=255, message="需求描述长度不能超过 255 个字符")
	public String getDemanddescribe() {
		return demanddescribe;
	}

	public void setDemanddescribe(String demanddescribe) {
		this.demanddescribe = demanddescribe;
	}
	
}