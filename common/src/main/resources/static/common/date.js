document.getElementsByTagName("body")[0].setAttribute('style',
		'height: 90% !important');
function HEIGHT() {
	var height = document.body.clientHeight - 132;
	var carouselInner = document.getElementsByClassName("carousel-inner")[0];
	carouselInner.style.height = height + "px";
	carouselInner.style.width = "100%";
}
HEIGHT();

// 获取日期并且格式化
var dvs = document.getElementById('dvs');
var d = new Date();
var y = d.getFullYear();
var m = d.getMonth() + 1;
var ds = d.getDate();
if (ds <= 9) {
	ds = '0' + ds;
}
var days = d.getDay();
switch (days) {
case 1:
	days = '星期一';
	break;
case 2:
	days = '星期二';
	break;
case 3:
	days = '星期三';
	break;
case 4:
	days = '星期四';
	break;
case 5:
	days = '星期五';
	break;
case 6:
	days = '星期六';
	break;
case 0:
	days = '星期日';
	break;
}
dvs.innerHTML = y + '-' + m + '-' + ds + ' ' + days;