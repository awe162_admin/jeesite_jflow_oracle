/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.anquan.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.anquan.entity.Anquan;

/**
 * anquanDAO接口
 * @author zz
 * @version 2019-02-12
 */
@MyBatisDao
public interface AnquanDao extends CrudDao<Anquan> {
	
}