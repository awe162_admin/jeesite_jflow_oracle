/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.daiban.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.daiban.entity.Daiban;

/**
 * 代办DAO接口
 * @author 曾
 * @version 2019-02-18
 */
@MyBatisDao
public interface DaibanDao extends CrudDao<Daiban> {
	
}