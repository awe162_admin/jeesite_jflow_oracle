/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.anquan.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.anquan.entity.Anquan;
import com.jeesite.modules.anquan.dao.AnquanDao;

/**
 * anquanService
 * @author zz
 * @version 2019-02-12
 */
@Service
@Transactional(readOnly=true)
public class AnquanService extends CrudService<AnquanDao, Anquan> {
	
	/**
	 * 获取单条数据
	 * @param anquan
	 * @return
	 */
	@Override
	public Anquan get(Anquan anquan) {
		return super.get(anquan);
	}
	
	/**
	 * 查询分页数据
	 * @param anquan 查询条件
	 * @param anquan.page 分页对象
	 * @return
	 */
	@Override
	public Page<Anquan> findPage(Anquan anquan) {
		return super.findPage(anquan);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param anquan
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Anquan anquan) {
		super.save(anquan);
	}
	
	/**
	 * 更新状态
	 * @param anquan
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Anquan anquan) {
		super.updateStatus(anquan);
	}
	
	/**
	 * 删除数据
	 * @param anquan
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Anquan anquan) {
		super.delete(anquan);
	}
	
}