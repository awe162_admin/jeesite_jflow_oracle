/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.TreeService;
import com.jeesite.modules.sys.entity.Caidan;
import com.jeesite.modules.sys.dao.CaidanDao;

/**
 * 菜单表Service
 * @author 曾
 * @version 2019-02-13
 */
@Service
@Transactional(readOnly=true)
public class CaidanService extends TreeService<CaidanDao, Caidan> {
	
	/**
	 * 获取单条数据
	 * @param caidan
	 * @return
	 */
	@Override
	public Caidan get(Caidan caidan) {
		return super.get(caidan);
	}
	
	/**
	 * 查询列表数据
	 * @param caidan
	 * @return
	 */
	@Override
	public List<Caidan> findList(Caidan caidan) {
		return super.findList(caidan);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param caidan
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Caidan caidan) {
		super.save(caidan);
	}
	
	/**
	 * 更新状态
	 * @param caidan
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Caidan caidan) {
		super.updateStatus(caidan);
	}
	
	/**
	 * 删除数据
	 * @param caidan
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Caidan caidan) {
		super.delete(caidan);
	}
	
	//一级菜单
	public List<Caidan> findParentName(Caidan caidan) {
		return dao.findParentName(caidan);
	}
		
	//二级菜单
	public List<Caidan> findSecond(@Param("menuCode") String menuCode) {
		return dao.findSecond(menuCode);
	}
			
	//所有菜单
	public List<Caidan> findAll(Caidan caidan) {
		return dao.findAll(caidan);
	}
	
	//所有url菜单
	public List<Caidan> findAllUrl(Caidan caidan) {
		return dao.findAllUrl(caidan);
	}
}