/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.staffadjust.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.staffadjust.entity.Staffadjust;
import com.jeesite.modules.staffadjust.service.StaffadjustService;

/**
 * staffadjustController
 * @author liuchang
 * @version 2019-02-19
 */
@Controller
@RequestMapping(value = "${adminPath}/staffadjust/staffadjust")
public class StaffadjustController extends BaseController {

	@Autowired
	private StaffadjustService staffadjustService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Staffadjust get(String id, boolean isNewRecord) {
		return staffadjustService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("staffadjust:staffadjust:view")
	@RequestMapping(value = {"list", ""})
	public String list(Staffadjust staffadjust, Model model) {
		model.addAttribute("staffadjust", staffadjust);
		return "modules/staffadjust/staffadjustList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("staffadjust:staffadjust:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Staffadjust> listData(Staffadjust staffadjust, HttpServletRequest request, HttpServletResponse response) {
		staffadjust.setPage(new Page<>(request, response));
		Page<Staffadjust> page = staffadjustService.findPage(staffadjust);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("staffadjust:staffadjust:view")
	@RequestMapping(value = "form")
	public String form(Staffadjust staffadjust, Model model) {
		model.addAttribute("staffadjust", staffadjust);
		return "modules/staffadjust/staffadjustForm";
	}

	/**
	 * 保存人员调班
	 */
	@RequiresPermissions("staffadjust:staffadjust:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Staffadjust staffadjust) {
		staffadjustService.save(staffadjust);
		return renderResult(Global.TRUE, text("保存人员调班成功！"));
	}
	
	/**
	 * 删除人员调班
	 */
	@RequiresPermissions("staffadjust:staffadjust:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Staffadjust staffadjust) {
		staffadjustService.delete(staffadjust);
		return renderResult(Global.TRUE, text("删除人员调班成功！"));
	}
	
}