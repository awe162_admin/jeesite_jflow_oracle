/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.arrangeclass.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.arrangeclass.entity.ArrangeClass;

/**
 * ArrangeclassDAO接口
 * @author liuchang
 * @version 2019-02-19
 */
@MyBatisDao
public interface ArrangeClassDao extends CrudDao<ArrangeClass> {
	
}