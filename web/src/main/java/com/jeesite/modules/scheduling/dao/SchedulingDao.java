/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.scheduling.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.scheduling.entity.Scheduling;

/**
 * 班次DAO接口
 * @author liuchang
 * @version 2019-02-19
 */
@MyBatisDao
public interface SchedulingDao extends CrudDao<Scheduling> {
	
}