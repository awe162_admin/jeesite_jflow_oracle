/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.daiban.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 代办Entity
 * @author 曾
 * @version 2019-02-18
 */
@Table(name="daiban", alias="a", columns={
		@Column(name="id", attrName="id", label="主键", isPK=true),
		@Column(name="leixing", attrName="leixing", label="类型"),
		@Column(name="renwu", attrName="renwu", label="任务"),
		@Column(name="title", attrName="title", label="标题", queryType=QueryType.LIKE),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class Daiban extends DataEntity<Daiban> {
	
	private static final long serialVersionUID = 1L;
	private String leixing;		// 类型
	private String renwu;		// 任务
	private String title;		// 标题
	
	public Daiban() {
		this(null);
	}

	public Daiban(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="类型长度不能超过 255 个字符")
	public String getLeixing() {
		return leixing;
	}

	public void setLeixing(String leixing) {
		this.leixing = leixing;
	}
	
	@Length(min=0, max=255, message="任务长度不能超过 255 个字符")
	public String getRenwu() {
		return renwu;
	}

	public void setRenwu(String renwu) {
		this.renwu = renwu;
	}
	
	@Length(min=0, max=255, message="标题长度不能超过 255 个字符")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}