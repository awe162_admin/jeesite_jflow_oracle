/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.entity;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;
import com.jeesite.common.entity.Extend;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.entity.TreeEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 菜单表Entity
 * @author 曾
 * @version 2019-02-13
 */
@Table(name="${_prefix}sys_menu", alias="a", columns={
		@Column(name="menu_code", attrName="menuCode", label="菜单编码", isPK=true),
		@Column(includeEntity=TreeEntity.class),
		@Column(name="menu_name", attrName="menuName", label="菜单名称", queryType=QueryType.LIKE),
		@Column(name="menu_type", attrName="menuType", label="菜单类型", comment="菜单类型（1菜单 2权限 3开发）"),
		@Column(name="menu_href", attrName="menuHref", label="链接"),
		@Column(name="menu_target", attrName="menuTarget", label="目标"),
		@Column(name="menu_icon", attrName="menuIcon", label="图标"),
		@Column(name="menu_color", attrName="menuColor", label="颜色"),
		@Column(name="permission", attrName="permission", label="权限标识"),
		@Column(name="weight", attrName="weight", label="菜单权重"),
		@Column(name="is_show", attrName="isShow", label="是否显示", comment="是否显示（1显示 0隐藏）"),
		@Column(name="sys_code", attrName="sysCode", label="归属系统", comment="归属系统（default:主导航菜单、mobileApp:APP菜单）"),
		@Column(name="module_codes", attrName="moduleCodes", label="归属模块", comment="归属模块（多个用逗号隔开）"),
		@Column(includeEntity=DataEntity.class),
		@Column(includeEntity=Extend.class, attrName="extend"),
	}, orderBy="a.tree_sorts, a.menu_code"
)
public class Caidan extends TreeEntity<Caidan> {
	
	private static final long serialVersionUID = 1L;
	private String menuCode;		// 菜单编码
	private String menuName;		// 菜单名称
	private String menuType;		// 菜单类型（1菜单 2权限 3开发）
	private String menuHref;		// 链接
	private String menuTarget;		// 目标
	private String menuIcon;		// 图标
	private String menuColor;		// 颜色
	private String permission;		// 权限标识
	private Integer weight;		// 菜单权重
	private String isShow;		// 是否显示（1显示 0隐藏）
	private String sysCode;		// 归属系统（default:主导航菜单、mobileApp:APP菜单）
	private String moduleCodes;		// 归属模块（多个用逗号隔开）
	private Extend extend;		// 扩展字段
	
	public Caidan() {
		this(null);
	}

	public Caidan(String id){
		super(id);
	}
	
	@Override
	public Caidan getParent() {
		return parent;
	}

	@Override
	public void setParent(Caidan parent) {
		this.parent = parent;
	}
	
	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}
	
	@NotBlank(message="菜单名称不能为空")
	@Length(min=0, max=100, message="菜单名称长度不能超过 100 个字符")
	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	
	@NotBlank(message="菜单类型不能为空")
	@Length(min=0, max=1, message="菜单类型长度不能超过 1 个字符")
	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}
	
	@Length(min=0, max=1000, message="链接长度不能超过 1000 个字符")
	public String getMenuHref() {
		return menuHref;
	}

	public void setMenuHref(String menuHref) {
		this.menuHref = menuHref;
	}
	
	@Length(min=0, max=20, message="目标长度不能超过 20 个字符")
	public String getMenuTarget() {
		return menuTarget;
	}

	public void setMenuTarget(String menuTarget) {
		this.menuTarget = menuTarget;
	}
	
	@Length(min=0, max=100, message="图标长度不能超过 100 个字符")
	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}
	
	@Length(min=0, max=50, message="颜色长度不能超过 50 个字符")
	public String getMenuColor() {
		return menuColor;
	}

	public void setMenuColor(String menuColor) {
		this.menuColor = menuColor;
	}
	
	@Length(min=0, max=1000, message="权限标识长度不能超过 1000 个字符")
	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}
	
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@NotBlank(message="是否显示不能为空")
	@Length(min=0, max=1, message="是否显示长度不能超过 1 个字符")
	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}
	
	@NotBlank(message="归属系统不能为空")
	@Length(min=0, max=64, message="归属系统长度不能超过 64 个字符")
	public String getSysCode() {
		return sysCode;
	}

	public void setSysCode(String sysCode) {
		this.sysCode = sysCode;
	}
	
	@NotBlank(message="归属模块不能为空")
	@Length(min=0, max=500, message="归属模块长度不能超过 500 个字符")
	public String getModuleCodes() {
		return moduleCodes;
	}

	public void setModuleCodes(String moduleCodes) {
		this.moduleCodes = moduleCodes;
	}
	
	public Extend getExtend() {
		return extend;
	}

	public void setExtend(Extend extend) {
		this.extend = extend;
	}
	
}